const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productoSchema = new Schema({

    nombre: String,
    marca: String,
    precio: Number,
    descripcion: String,
    stock: Number,
    estado: { type: String, default:'Vigente' },
    fechaCreacion: { type: Date, default: Date.now() },
    imagenPath: String
});

module.exports = mongoose.model('Producto', productoSchema, 'productos');