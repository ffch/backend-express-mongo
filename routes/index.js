var express = require('express');
var router = express.Router();

const Producto = require('./../model/producto');
const multer = require('../config/multer');
const path = require('path');
const fs = require('fs-extra');
const axios = require('axios').default;

/* GET home page. */
router.get('/', function (req, res, next) {
  res.send(
    "holaasdfasdf"
  );
});

//obtener productos vigentes desde bodega y productos ANWO
router.get('/productos-vigentes', async function (req, res, next) {

  let productosANWO;
  try {
    const productosVigentes = await Producto.find({
      $and: [{
        estado: 'Vigente'
      }]
    });

    axios.get('http://23.23.205.206:3000/productos')
    .then(function(response){

      productosANWO = response.data;

      productosANWO.forEach(element => {
        productosVigentes.push(element);
      });

      res.status(200).json({
        productosVigentes
      });
    })
    .catch(function(error){

      console.log('error axios', error);
      res.status(500).json({
        errorAxios: error
      })
    })


  } catch (error) {
    console.log(error);
    res.status(500).json({
      error: error
    })
  }

});

//obtener todos los productos desde bodega
router.get('/productos', async function (req, res, next) {

  const productos = await Producto.find();

  return res.status(200).json(productos);

});

//crear producto
router.route('/productos').post(multer.single('img') , function (req, res, next) {

  console.log('datos de entrada ', req.body);
  console.log('datos del archivo', req.file);

  const producto = new Producto({

    nombre: req.body.nombre,
    marca: req.body.marca,
    precio: Number(req.body.precio),
    descripcion: req.body.descripcion,
    stock: Number(req.body.stock),
    imagenPath: req.file.path
  });

  console.log('producto antes de registrar ', producto);

  producto.save(function(err, respuesta){

    if(err){
      res.send(err);
    }else{
      res.status(200).json(respuesta);
    }
  });

});

//obtener producto por id
router.get('/productos/:id', function (req, res, next) {

  Producto.findById(req.params.id, {

  }, function(err, respuesta){

    if(err){
      res.send(err);
    }else{
      console.log('respuesta de producto ', respuesta);
      res.status(200).json(respuesta);
    }
  });

});

//eliminar producto por id
router.delete('/productos/:id', async function (req, res, next) {

  const producto = await Producto.findByIdAndRemove(req.params.id);

  if(producto.imagenPath != null && producto.imagenPath != ''){
    await fs.unlink(path.resolve(producto.imagenPath));
  }

  return res.status(200).json({
    mensaje: 'Producto eliminado correctamente',
    producto
  });

});

//actualizar producto por id
router.put('/productos/:id', async function (req, res, next) {

  console.log('Producto a actualizar ', req.body);

  const { estado, nombre, marca, precio, stock, descripcion } = req.body;
  
  if(marca != 'ANWO'){
    const productoActualizado = await Producto.findByIdAndUpdate(req.params.id, {
      estado, nombre, marca, precio, stock, descripcion 
    }, {new:true});
  
    res.status(201).json({
      message: 'Producto actualizado correctamente',
      productoActualizado
    })
  }else{

    //const producto = req.body;
    //console.log('producto para axios ', req);
    axios.put('http://23.23.205.206:3000/productos/' + req.params.id, { 
      "estado": estado, 
      "nombre": nombre, 
      "marca": marca, 
      "precio":precio, 
      "stock":stock,
      "descripcion":descripcion })
    .then(function(response){
      console.log('respuesta desde servidor de ANWO ', response);
      res.status(201).json({
        respuesta: response.data
      });
    })
    .catch(function(error){

      console.log('error axios', error);
      res.status(500).json({
        errorAxios: error
      })
    })
  }
  

});

module.exports = router;
