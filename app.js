var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
const cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//habilitamos cors para todas las rutas
app.use(cors());
//indicamos que los datos se enviaran en formato json
app.use(express.json());
//indicamos que la carpeta uploads va a poder ser accedida desde el browser
app.use('/uploads', express.static(path.resolve('uploads')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

mongoose.connect('mongodb://54.156.129.132:27117/buenos-aires', {
    useUnifiedTopology:true,
    useNewUrlParser:true,
    useFindAndModify: false
});

module.exports = app;
